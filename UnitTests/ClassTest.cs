using System;
using CodeDom.Fluent;
using Xunit;

namespace UnitTests
{
    public class ClassTest
    {
        [Fact]
        public void EmptyClass()
        {
            Class testClass = Class.Create("TestClass");
            Assert.Equal(@"TestClass{}", testClass.ToString());
        }
    }
}