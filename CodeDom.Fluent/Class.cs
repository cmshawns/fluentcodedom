﻿using System;
using System.CodeDom;

namespace CodeDom.Fluent
{
    public class Class
    {
        private readonly CodeTypeDeclaration _typeDeclaration;

        private Class(string name)
        {
            _typeDeclaration = new CodeTypeDeclaration(name);
        }

        public static Class Create(string name)
        {
            Class c = new Class(name);
            return c;
        }

        public override string ToString()
        {
            return _typeDeclaration.ToString();
        }
    }
}